# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 10:41:48 2020

@author: Inês
"""
import numpy as np
import itertools as it
import random

def house_of_cards (l):
    """
    Generates a fitness landscape based on a House-of-Cards model.
    l : number of loci
    """
    mut = np.array(list(it.product([0, 1], repeat=l)))
    fit = np.random.normal(size = (2**l, 1))
    mat = np.hstack((mut, fit))
    return mat

# print (house_of_cards(3))

def additive_model (l):
    """
    Generates a fitness landscape based on an additive model.
    l : number of loci
    """
    mut = np.array(list(it.product([0, 1], repeat=l)))
    fit = np.random.normal(size = l)
    fit_base = np.zeros((2**l, 1)) + np.random.normal()
    mat = np.hstack((mut, fit_base))
    for i in range(2**l):
        for j in range(l):
            if mat [i, j] == 1:
                mat [i, l] += fit[j]
    return mat

# print (additive_model(3))

def rough_mt_fuji (l, r=1):
    """
    Generates a fitness landscape based on a Rough Mt Fuji model
    l : number of loci
    r : standard deviation of the epistatic component
        The standard deviation of the additive component is 1.
    """
    mat = additive_model(l)
    for i in range(2**l):
        mat [i, l] += np.random.normal(scale = r)
    return mat

# print (rough_mt_fuji(3))

def normalize_landscape (landscape):
    """
    Normalizes a fitness landscape vector as to achieve a zero mean, unit
    variance vector.
    landscape : fitness landscape vector
    """
    return (landscape - np.mean(landscape)) / np.std(landscape)

def orthogonalize (l1, l2):
    """
    Makes a fitness landscape vector (l2) orthogonal to another (l1).
    l1, l2 : fitness landscape vectors
    """
    return l1 - ((np.dot(l2, l1)) / (np.linalg.norm(l2)) ** 2) * l2

def more_fitness_landscapes (l, n, a=0.5, r=1):
    """
    Generates several fitness landscapes with an adjustable correlation degree
    to the first one by generalizing the procedure described in:
    https://doi.org/10.1101/803619
    l : number of loci
    n : number of environments
    a : correlation degree between environments
    r : standard deviation of the epistatic component
    """
    l1 = rough_mt_fuji(l, r)[:,-1]
    l1_norm = normalize_landscape(l1)
    ortho = np.atleast_2d(l1_norm)
    result = np.atleast_2d(l1)
    for i in range(1, n):
        li = rough_mt_fuji(l, r)[:,-1]
        mean_li = np.mean(li)
        std_li = np.std(li)
        li = normalize_landscape(li)
        for j in range(result.shape[0]):
            li = orthogonalize(li,  ortho[j])
        li = normalize_landscape(li)
        ortho = np.vstack((ortho, li))
        li = (l1_norm*a + li*(1-a**2)**(1/2)) * std_li + mean_li
        result = np.vstack((result, li))
    return result

print (more_fitness_landscapes(5, 2))

# n = 5
# landscapes = more_fitness_landscapes(3, n)
# print('inner product:')
# for i in range(n):
#     for j in range(n):
#         print(f'{np.dot(landscapes[i], landscapes[j]):.3}', end='\t')
#     print()
# print('correlations:')
# for i in range(n):
#     for j in range(n):
#         print(f'{np.corrcoef(landscapes[i], landscapes[j])[0,1]:.3}', end='\t')
#     print()

def landscape_gradient (l, n, r=1):
    """
    Generates a gradient of fitness landscapes.
    l : number of loci
    n : number of environments
    r : standard deviation of the epistatic component
    """
    ldsc_n = np.linspace(0, 1, n)
    l1 = normalize_landscape(rough_mt_fuji(l, r)[:,-1])
    l2 = normalize_landscape(rough_mt_fuji(l, r)[:,-1])
    l2 = orthogonalize(l2, l1)
    l2 = normalize_landscape(l2)
    result = l1
    for i in range(1, n-1):
        li =  l2*ldsc_n[i] + l1*(1-ldsc_n[i])
        result = np.vstack((result, li))
    result = np.vstack((result, l2))
    return result

# print (landscape_gradient(3, 5))



def calc_fitness_andre (environment, phenotype, c):
    """
    Calculates the fitness based on André's model for one enviroment and 
    phenotype.
    environment, phenotype : vector with d dimensions
    c : constant
    """
    fitness = c + np.dot(environment, phenotype) - np.linalg.norm(phenotype) ** 2
    return fitness

# print (calc_fitness_andre(np.random.normal(size = 2), np.random.normal    \
#         (size = 2), np.random.normal()))

def calc_fitness_fgm (environment, phenotype, c):
    """
    Calculates the fitness using Fisher's Geometric Model (gaussian model).
    environment, phenotype : vector with d dimensions
    c : constant
    """
    return c - np.linalg.norm(phenotype - environment) ** 2

# print (calc_fitness_fgm(np.random.normal(size = 2), np.random.normal    \
#        (size = 2), np.random.normal()))

def calc_fitness_add (phenotypes):
    """"
    Generates a fitness landscape based on an additive model.
    phenotypes : list with the values of f1, ... fl, fbase
    """
    l = len(phenotypes) - 1
    gen = np.array(list(it.product([0, 1], repeat=l)))
    fit_base = np.zeros((2**l, 1)) + phenotypes[l]
    mat = np.hstack((gen, fit_base))
    for i in range(2**l):
        for j in range(l):
            if mat [i, j] == 1:
                mat [i, l] += phenotypes[j]
    return mat[:,l]

# print(calc_fitness_add(np.random.normal(size = 4)))

def calc_fitnesses (environments, phenotypes, c, model):
    """
    Calculates the fitness based on André's model or FGM for several 
    environments and phenotypes.
    environments : array of n vectors of d dimensions
    phenotypes : array of 2**l vectors of d dimensions
    c : constant
    model : model used for calculating fitness (André's model or FGM)
    """
    fit = np.zeros((environments.shape[0], phenotypes.shape[0]))
    for e in range(environments.shape[0]):
        for p in range(phenotypes.shape[0]):
            fit [e, p] = model(environments[e], phenotypes[p], c[e])
    return fit

# print (calc_fitnesses(np.random.normal(size = (5, 2)), np.random.normal    \
#     (size = (8, 2)), np.random.normal(size = 5), calc_fitness_andre))

def calc_fitnesses_add (phenotypes):
    """
    Calculates the fitness based on an additive model for several environments.
    phenotypes : list with the values of f1, ... fl, fbase for each environment
    """
    fit = np.zeros((phenotypes.shape[0], 2**(phenotypes.shape[1]-1)))
    for i in range(phenotypes.shape[0]):
        fit[i] = calc_fitness_add(phenotypes[i])
    return fit

# print (calc_fitnesses_add(np.random.normal(size = (5, 4))))

def calc_error (fit_mod, fit_exp):
    """
    Calculates the error that is used to fit our data to the experimental
    model. This value shows how well adjusted our model is to the experimental
    data.
    fit_mod : fitness calculated by our model
    fit_exp : fitness of our simulated experimental data
    """
    new_mat = (fit_mod - fit_exp) ** 2
    return np.sum(new_mat)

# mod = calc_fitnesses(np.random.normal(size = (5, 2)), np.random.normal    \
#         (size = (8, 2)), np.random.normal(size = 5), calc_fitness_andre)
# exp = more_fitness_landscapes(3, 5)
# print (calc_error(mod, exp))

def normalize_error_andre (error, l, n):
    points = n*2**l
    parameters = 2*2**l + 2*n
    return error / (points - parameters)

def normalize_error_fgm (error, l, n):
    points = n*2**l
    parameters = 2*2**l + 3*n
    return error / (points - parameters)

def normalize_error_add (error, l, n):
    points = n*2**l
    parameters = n*(l+1)
    return error / (points - parameters)

def fit_model (environments, phenotypes, fit_exp, c, model):
    """
    Improves the model's fit by adding small numbers to the environments and
    phenotypes.
    environments : array of n vectors of d dimensions
    phenotypes : array of 2**l vectors of d dimensions
    fit_exp : fitness of our simulated experimental data
    """
    best_env = environments
    best_phn = phenotypes
    best_c = c
    best_error = calc_error(calc_fitnesses(environments, phenotypes, c, model),\
                            fit_exp)
    for _ in range(50000):
        smalln1 = np.random.normal(scale = 0.001,    \
                  size = (environments.shape[0], environments.shape[1]))
        smalln2 = np.random.normal(scale = 0.001,    \
                  size = (phenotypes.shape[0], phenotypes.shape[1]))
        smalln3 = np.random.normal(scale = 0.001, size = (environments.shape[0]))
        new_env = best_env + smalln1
        new_phn = best_phn + smalln2
        new_c = best_c + smalln3
        new_error = calc_error(calc_fitnesses(new_env, new_phn, new_c, model),\
                               fit_exp)
        if new_error < best_error:
            best_error = new_error
            best_env = new_env
            best_phn = new_phn
            best_c = new_c
    return best_env, best_phn, best_c, best_error

# env = np.random.normal(size = (5, 2))
# phn = np.random.normal(size = (8, 2))
# c = np.random.normal(size = 5)
# exp = more_fitness_landscapes(3, 5)
# print (fit_model(env, phn, exp, c, calc_fitness_andre))

def fit_model_add (phenotypes, fit_exp):
    """
    Improves the model's fit by adding small numbers to f1, ... fl, fbase
    phenotypes : list with the values of f1, ... fl, fbase for each environment
    fit_exp : fitness of our simulated experimental data
    """
    best_phn = phenotypes
    fit_mod = calc_fitnesses_add(phenotypes)
    best_error = calc_error(fit_mod, fit_exp)
    for _ in range(50000):
        smalln = np.random.normal(scale = 0.001,    \
                 size = (phenotypes.shape[0], phenotypes.shape[1]))
        new_phn = best_phn + smalln
        new_error = calc_error(calc_fitnesses_add(new_phn), fit_exp)
        if new_error < best_error:
            best_error = new_error
            best_phn = new_phn
    return best_phn, best_error

# print (fit_model_add(np.random.normal(size = (5, 4)),    \
#                   more_fitness_landscapes(3, 5)))

def run_model_andre (l, n, a=0.5, r=1):
    exp = more_fitness_landscapes(l, n, a, r)
    env = np.random.normal(size = (n, 2))
    phn = np.random.normal(size = (2**l, 2))
    c = np.random.normal(size = n)
    env, phn, c, err = fit_model(env, phn, exp, c, calc_fitness_andre)
    mod = calc_fitnesses(env, phn, c, calc_fitness_andre)
    return exp, mod, normalize_error_andre(err, l, n)

# print (run_model_andre(5, 5))

def run_model_fgm (l, n, a = 0.5, r = 1):
    exp = more_fitness_landscapes(l, n, a, r)
    env = np.random.normal(size = (n, 2))
    phn = np.random.normal(size = (2**l, 2))
    c = np.random.normal(size = n)
    env, phn, c, err = fit_model(env, phn, exp, c, calc_fitness_fgm)
    mod = calc_fitnesses(env, phn, c, calc_fitness_fgm)
    return exp, mod, normalize_error_fgm(err, l, n)

# print (run_model_fgm(5, 5))

def run_model_add (l, n, a = 0.5, r = 1):
    exp = more_fitness_landscapes(l, n, a, r)
    phn = np.random.normal(size = (n, l+1))
    phn, err = fit_model_add(phn, exp)
    mod = calc_fitnesses_add(phn)
    return exp, mod, normalize_error_add(err, l, n)

# print (run_model_add(3, 3))



def fit_environments (environments, phenotypes, fit_exp, c, model):
    best_env = environments
    best_c = c
    best_error = calc_error(calc_fitnesses(environments, phenotypes, c, model),\
                            fit_exp)
    for _ in range(50000):
        smalln1 = np.random.normal(scale = 0.001,    \
                  size = (environments.shape[0], environments.shape[1]))
        smalln2 = np.random.normal(scale = 0.001, size = (environments.shape[0]))
        new_env = best_env + smalln1
        new_c = best_c + smalln2
        new_error = calc_error(calc_fitnesses(new_env, phenotypes, new_c, model),\
                               fit_exp)
        if new_error < best_error:
            best_error = new_error
            best_env = new_env
            best_c = new_c
    return best_env, best_c, best_error

def fit_without_some_env (environments, phenotypes, fit_exp, c, model,\
                             env_number, gen_number):
    first_env = environments[0:-env_number]
    first_exp = fit_exp[0:-env_number]
    first_c = c[0:-env_number]
    fitted_first_env, fitted_phn, fitted_first_c, err =    \
        fit_model(first_env, phenotypes, first_exp, first_c, model)
    gen_ind = random.sample(range(fitted_phn.shape[0]), gen_number)
    last_env = np.atleast_2d(environments[-env_number:])
    last_phn = np.atleast_2d(fitted_phn[gen_ind])
    last_exp = np.atleast_2d(fit_exp[-env_number:, gen_ind])
    last_c = c[-env_number:]
    fitted_last_env, fitted_last_c, error = fit_environments(last_env,    \
                                            last_phn, last_exp, last_c, model)
    fitted_env = np.vstack((fitted_first_env, fitted_last_env))
    fitted_c = np.hstack((fitted_first_c, fitted_last_c))
    return fitted_env, fitted_phn, fitted_c

# env = np.random.normal(size = (5, 2))
# phn = np.random.normal(size = (8, 2))
# exp = more_fitness_landscapes(3, 5)
# c = np.random.normal(size = 5)
# print (fit_without_some_env(env, phn, exp, c, calc_fitness_andre, 1, 4))

def run_model_without_some_env (l, n, a, r, model, env_number, gen_number):
    exp = more_fitness_landscapes(l, n, a, r)
    env = np.random.normal(size = (n, 2))
    phn = np.random.normal(size = (2**l, 2))
    c = np.random.normal(size = n)
    env, phn, c = fit_without_some_env(env, phn, exp, c, model, env_number,    \
                                    gen_number)
    mod = calc_fitnesses(env, phn, c, model)
    return exp, mod

# print(run_model_without_some_env(3, 5, 0.5, 1, calc_fitness_andre, 2, 4))



def r_squared (exp, mod):
    """
    Coefficient of determination: measure of similarity between the
    "experimental" data and the data calculated by our model.
    exp : fitness landscapes from the simulated experimental data
    mod : fitness landscapes generated by the model
    """
    sum_1 = np.sum((exp - mod) ** 2)
    sum_2 = np.sum((exp - np.mean(exp)) ** 2)
    return 1 - (sum_1 / sum_2)

# env = np.random.normal(size = (5, 2))
# phn = np.random.normal(size = (8, 2))
# exp = more_fitness_landscapes(3, 5)
# env, phn, error = fit_model(env, phn, exp)
# print (r_squared(exp, calc_fitnesses(env, phn)))

# a = np.array([3*np.arange(20) + 2 + np.random.normal(0,0.01,size=(20))    \
#               for _ in range(3)])
# b = np.array([3*np.arange(20) + 2 for _ in range(3)])
# print(r_squared(a,b))

def corr_coef (exp, mod):
    """
    Calculates the correlation coefficient.
    exp : fitness landscapes from the simulated experimental data
    mod : fitness landscapes generated by the model
    """
    return np.corrcoef(exp.flatten(), mod.flatten())[0, 1]

def mutate_locus (genotype, position):
    genotype = np.copy(genotype)
    genotype[position] = not genotype[position]
    return genotype

# print (mutate_locus(np.array([1, 0, 1, 0]), 1))

def fitness_effect (landscape, mut_gen, gen):
    base = np.flip(2**np.arange(landscape.shape[1]-1))
    ind_gen = np.dot(gen, base)
    ind_mut_gen = np.dot(mut_gen, base)
    fit_gen = landscape[int(ind_gen), landscape.shape[1]-1]
    fit_mut_gen = landscape[int(ind_mut_gen), landscape.shape[1]-1]
    return fit_mut_gen - fit_gen

# print (fitness_effect(rough_mt_fuji(3), np.array([0.0, 0.0, 0.0]), \
#                       np.array([0.0, 1.0, 0.0])))

def gamma (l, landscape):
    """
    Calculates gamma, a measure of the amount of epistasis in fitness
    landscapes.
    l : number of loci
    landscape : fitness landscape (with genotypes)
    """
    num = 0
    den = 0
    for g in range(landscape.shape[0]):
        gen = landscape[g, 0:-1]
        for j in range(l):
            genj = mutate_locus(gen, j)
            sjg = fitness_effect(landscape, genj, gen)
            den += sjg**2
            for i in range(l):
                if j != i:
                    geni = mutate_locus(gen, i)
                    genij = mutate_locus(genj, i)
                    sjgi = fitness_effect(landscape, genij, geni)
                    num += sjg * sjgi
    den = den * (l - 1)
    return num / den

# for r in [0, 0.5, 0.75, 1, 1.5, 2]:
#     g = np.zeros(500)
#     for i in range(500):
#         g[i] = gamma(5, rough_mt_fuji(5, r))
#     # print (g)
#     print(r, g.mean(), 1 - 2*r**2/(1+2*r**2), g.std()/np.sqrt(500))

def gamma_test (l, n, landscapes):
    """
    Calculates the mean of the gammas in different environments.
    l : number of loci
    n : number of environments
    landscapes : fitness landscapes (exp or mod)
    """
    gen = np.array(list(it.product([0, 1], repeat=l)))
    gamma_vals = []
    for i in range(n):
        ldsc = np.hstack((gen, landscapes[i].reshape((2**l, 1))))
        gamma_vals.append(gamma(l, ldsc))
    gamma_mean = np.mean(gamma_vals)
    return gamma_mean

# print (gamma_test(3, 5, np.random.normal(size = (5,8))))



def prepare_arrays (parameters, rep, number):
    """
    Prepares the arrays to store results.
    parameters : list with the parameter that will change
    rep : number of repetitions of each test
    number : number of arrays
    """
    mat = np.zeros((len(parameters), rep))
    mat = np.hstack((mat, parameters.reshape(len(parameters), 1)))
    list = []
    for _ in range(number):
        list.append(mat)
    return np.array(list)

# print (prepare_arrays(np.array([2, 3]), 2, 3))

def write_to_file (filename, array):
    np.savetxt(filename, array)

# write_to_file('abc.txt', r_squared_env([2, 3], 3, 0.5, 10000, 2))

def file_to_array (filename):
    mat = np.loadtxt(filename)
    return mat

# print(file_to_array('abc.txt'))
    


def different_genotypes(dif_n, gen_n):
    check = [True for _ in range(dif_n)]
    mat = np.array(list(it.product([0, 1], repeat=dif_n)))
    while check != [False for _ in range(dif_n)]:
        np.random.shuffle(mat)
        mat2 = mat[0:gen_n]
        check = list(np.all(mat2 == mat2[0,:], axis=0))
    return mat2
  
# print (different_genotypes(dif_n=5, gen_n=4))

def choose_possible_genotypes(l, dif_n, gen_number):
    different = different_genotypes(dif_n, gen_number)
    equal = np.repeat(np.atleast_2d([random.randint(0,1)    \
                      for _ in range(l-dif_n)]), gen_number, axis = 0)
    print(different, equal) 
    gen = np.hstack((different, equal))
    gen = np.transpose(gen)
    np.random.shuffle(gen)
    gen = np.transpose(gen)
    # print (gen)
    base = np.flip(2**np.arange(l))
    genind = []
    for i in range(gen.shape[0]):
        genind.append(np.dot(gen[i], base))
    return genind

# print(choose_possible_genotypes(l=5, dif_n=3, gen_number=4))

def fit_without_some_env_diff (environments, phenotypes, fit_exp, c, model,\
                             env_number, gen_number, l, dif_n):
    first_env = environments[0:-env_number]
    first_exp = fit_exp[0:-env_number]
    first_c = c[0:-env_number]
    fitted_first_env, fitted_phn, fitted_first_c, err =    \
        fit_model(first_env, phenotypes, first_exp, first_c, model)
    gen_ind = choose_possible_genotypes(l, dif_n, gen_number)
    last_env = np.atleast_2d(environments[-env_number:])
    last_phn = np.atleast_2d(fitted_phn[gen_ind])
    last_exp = np.atleast_2d(fit_exp[-env_number:, gen_ind])
    last_c = c[-env_number:]
    fitted_last_env, fitted_last_c, error = fit_environments(last_env,    \
                                            last_phn, last_exp, last_c, model)
    fitted_env = np.vstack((fitted_first_env, fitted_last_env))
    fitted_c = np.hstack((fitted_first_c, fitted_last_c))
    return fitted_env, fitted_phn, fitted_c

def run_model_without_some_env_diff (l, n, a, r, dif_n, gen_number, model):
    exp = more_fitness_landscapes(l, n, a, r)
    env = np.random.normal(size = (n, 2))
    phn = np.random.normal(size = (2**l, 2))
    c = np.random.normal(size = n)
    env, phn, c2 = fit_without_some_env_diff(env, phn, exp, c, model, 1,    \
                                   gen_number, l, dif_n)
    mod = calc_fitnesses(env, phn, c2, model)
    return exp, mod

# print (run_model_without_some_env_diff(5, 5, 0.5, 1, 3, 4, calc_fitness_andre))